package com.junit_5_basics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

@DisplayName("Automated Test: When running MathUtils")
class AutomatedTest {
	
	MathUtils mathUtils;
	@BeforeEach
	void init() {
		mathUtils = new MathUtils();
	}

	@ParameterizedTest
//	@CsvSource({"0, 100,100"})
	@CsvFileSource(resources = "/com/junit_5_basics/values.csv")
	void automatedAdd(int min, int max, int sum) {
		int Min = min;
		int Max = max;
		int expected = sum;
		int actual = mathUtils.add(Min, Max);
		
		assertEquals(expected, actual,"The automated test: add method should add two numbers");
	}
	
	@RepeatedTest(100)
	void repeatedAutomatedAdd() {
		
		Random rd = new Random();
	   
		int num1 = rd.nextInt();
		int num2 = rd.nextInt();
		
		int expected = num1 + num2;
		int actual = mathUtils.add(num1, num2);
		
		System.out.println(num1);
		System.out.println(num2);
		assertEquals(expected, actual,"The automated test: add method should add two numbers");
	}
}
