package com.junit_5_basics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;


//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("TestLifeCycle: When running MathUtils")
class TestLifeCycle {
	
	MathUtils mathUtils;
	
	@BeforeAll
	static void beforeAllInit() {
		System.out.println("@BeforeAll: This runs before all the methods");
	}
	
	@BeforeEach
	void init() {
		System.out.println("@BeforeEach: This runs before each of the test methods");
		mathUtils = new MathUtils();
	}
	
	@AfterEach
	void cleanUp() {
		System.out.println("@AfterEach: Cleaning Up...");
	}
	
	@AfterAll
	static void afterAllFinalize() {
		System.out.println("@AfterAll: This runs after all the methods");
	}
	
	@Test
	@DisplayName("Testing add method")
	void testAdd() {
		
		int expected = 2;
		int actual = mathUtils.add(1, 1);
		System.out.println("The add method should add two numbers");
		assertEquals(expected, actual,"The add method should add two numbers");
	}
	
	@Test
	@DisplayName("Testing substract method")
	void testSubstract() {
		
		int expected = 10;
		int actual = mathUtils.substract(15, 5);
		System.out.println("The add method should substract two numbers");
		assertEquals(expected, actual,"The add method should substract two numbers");
	}
	
	@Test
	@DisplayName("Testing multiply method")
	void testMultipy() {
		
		int expected = 16;
		int actual = mathUtils.multiply(8, 2);
		System.out.println("The add method should muliply two numbers");
		assertEquals(expected, actual,"The add method should muliply two numbers");
	}
	
}
