package com.junit_5_suites;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectClasses({MathUtilsDoubleTest.class,MathUtilsFloatTest.class,MathUtilsIntTest.class})
public class AllUnitTest {
	
}
